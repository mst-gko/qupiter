# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMoveTools
                                 A QGIS plugin
 This plugin installs GeoMove Tools
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-01-03
        copyright            : (C) 2019 by GEUS
        email                : jla@geus.dk
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

__author__ = 'GEUS'
__date__ = '2019-01-03'
__copyright__ = '(C) 2019 by GEUS'


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load GeoMoveTools class from file GeoMoveTools.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .qupiter_plugin import QupiterPlugin
    return QupiterPlugin()
