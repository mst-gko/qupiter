# -*- coding: utf-8 -*-
"""
***************************************************************************
    well_algorithm.py
    -------------------------
    begin                : January 2019
    copyright            : (C) 2018 by Jakob Lanstorp
    email                : jla at geus dot dk
    dev for              : http://geus.dk/qupiter/
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Jakob Lanstorp'
__date__ = 'January 2019'
__copyright__ = '(C) 2019, Jakob Lanstorp'

import os

from PyQt5.QtGui import QIcon
from qgis.core import (
    QgsApplication,
    QgsProcessingAlgorithm)
from processing.tools.system import isWindows, isMac

class PlantAlgorithm(QgsProcessingAlgorithm):
    '''Base class for all GeoMove algorithms and scripts.'''

    # take care of this class variable in case of
    # multithread processing
    feedback = None
    # tag used in QgsMessageLog notifications
    messageTag = ''
    pipelinesPath = os.path.join(os.path.dirname(__file__), 'pdal_pipelines')

    def tr(self, string, context=''):
        if context == '':
            context = 'PlantAlgorithmProvider'
        return QgsApplication.translate(context, string)

    def icon(self):
        iconPath = os.path.join(os.path.dirname(__file__), 'geus_logo.png')
        return QIcon(iconPath)

    def flags(self):
        return QgsProcessingAlgorithm.FlagSupportsBatch | \
               QgsProcessingAlgorithm.FlagCanCancel

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(u'Anlæg')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return u'anlaeg'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(self.__doc__)
