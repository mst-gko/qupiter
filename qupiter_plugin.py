# -*- coding: utf-8 -*-

"""
/***************************************************************************
 QupiterPlugin
                                 A QGIS plugin
 This plugin installs Qupiter
                              -------------------
        begin                : 2019-01-03
        copyright            : (C) 2018 by GEUS
        email                : jla@geus.dk
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'GEUS'
__date__ = '2018-01-03'
__copyright__ = '(C) 2019 by GEUS'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os
import sys
import inspect

from qgis.core import QgsProcessingAlgorithm, QgsApplication
from .pcjupiterxl_provider import PCJupiterXLProvider

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


class QupiterPlugin(object):

    def __init__(self):
        self.provider = PCJupiterXLProvider()

    def initGui(self):
        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        # remove only if provider is available. For dependency reason
        # provier can be available but c++ part already deleted => try:catch
        try:
            QgsApplication.processingRegistry().removeProvider(self.provider)
        except Exception as ex:
            pass
