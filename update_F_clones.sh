#!/usr/bin/env bash
set -e

function update_to_latest_tag {
    echo "cd $1"
    cd "$1" || exit 1
    git fetch --tags
    latesttag="$(git describe --abbrev=0 --tags)"
    echo "Checking out $latesttag"
    git checkout "$latesttag"
}

update_to_latest_tag "$HOME/f/GKO/data/grukos/jupiter/qupiter/plugin/qupiter-qgis2"
update_to_latest_tag "$HOME/f/GKO/data/grukos/jupiter/qupiter/plugin/qupiter-qgis3"
