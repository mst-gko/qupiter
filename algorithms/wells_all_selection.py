# -*- coding: utf-8 -*-

"""
***************************************************************************
    wells_all.py
    -------------------------
    begin                : January 2019
    copyright            : (C) 2019 by Jakob Lanstorp
    email                : jla at geus dot dk
    dev for              : http://geus.dk/qupiter/
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Jakob Lanstorp'
__date__ = 'January 2019'
__copyright__ = '(C) 2019, Jakob Lanstorp'


import os
import re
import json
import subprocess
# import pdal # cannot import pdal because mosto stable versions just use python-pdal for py2
from PyQt5.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBand,
                       QgsProcessingParameterRange,
                       QgsProcessingParameterNumber,
                       QgsMessageLog,
                       QgsProcessingParameterDefinition,
                       QgsProcessingParameterVectorDestination,
                       QgsProcessingUtils)
import processing
from ..well_algorithm import WellAlgorithm
from ..pcjupiterxl_aux import PCJupiterXLAux as Aux


class WellsAllSelection(WellAlgorithm):
    """
    Get all boreholes within a selected geometry from another layer
    """
    VECTOR_SELECTION = 'VECTOR_SELECTION'
    OUTPUT = 'OUTPUT'

    def createInstance(self):
        self.messageTag = type(self).__name__ # e.g. string WellsAllSelection
        return WellsAllSelection()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'wellsallselection'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Hent alle boringer indenfor valgte geometri')
		
    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.VECTOR_SELECTION,
                self.tr(u'Table with selected feature for bounding geographic query'),
                [QgsProcessing.TypeVectorPolygon], 
                optional=False
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                name=self.OUTPUT,
                description=self.tr('Output table')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        bounding_selection_layer = self.parameterAsSource(
            parameters, 
            self.VECTOR_SELECTION, 
            context)
        
        output_vector = self.parameterAsOutputLayer(
            parameters,
           self.OUTPUT,
            context
        )
        
        if bounding_selection_layer is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.VECTOR_SELECTION))

        select_vector_path = None
        if isinstance(output_vector, str):
            select_vector_path = bounding_selection_layer
        else:
            select_vector_path = bounding_selection_layer.source()
            
        out_vector_path = None
        if isinstance(output_vector, str):
            out_vector_path = output_vector
        else:
            out_vector_path = output_vector.source()
        
        # Type check
        Aux.log_info('type(bounding_selection_layer): {}'.format(type(select_vector_path)))
        Aux.log_info('type(out_vector_path): {}'.format(type(out_vector_path)))
        
        # Paths
        Aux.log_info('bounding_selection_layer: {}'.format(select_vector_path))
        Aux.log_info('out_vector_path: {}'.format(out_vector_path))

        # Return the results of the algorithm
        return {self.OUTPUT: out_vector_path}
