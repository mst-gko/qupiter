# -*- coding: utf-8 -*-

"""
***************************************************************************
    plants_all.py
    -------------------------
    begin                : January 2019
    copyright            : (C) 2019 by Jakob Lanstorp
    email                : jla at geus dot dk
    dev for              : http://geus.dk/qupiter/
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Jakob Lanstorp'
__date__ = 'January 2019'
__copyright__ = '(C) 2019, Jakob Lanstorp'


import os
import re
import json
import subprocess
# import pdal # cannot import pdal because mosto stable versions just use python-pdal for py2
from PyQt5.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterBand,
                       QgsProcessingParameterRange,
                       QgsProcessingParameterNumber,
                       QgsMessageLog,
                       QgsProcessingParameterDefinition,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingUtils)
import processing
from ..plant_algorithm import PlantAlgorithm


class PlantsAll(PlantAlgorithm):
    """
    Query all wells i.e. all boreholes
    """

    INPUT_PCL = 'INPUT_PCL'
    INPUT_CROSSWALK_CLASSES = 'INPUT_CROSSWALK_CLASSES'
    INPUT_CROSSWALK_CLASSES_BAND = 'INPUT_CROSSWALK_CLASSES_BAND'
    INPUT_PCL_S1_INTENSITY_RANGE = 'INPUT_PCL_S1_INTENSITY_RANGE'
    INPUT_PCL_S2_INTENSITY_RANGE = 'INPUT_PCL_S2_INTENSITY_RANGE'
    INPUT_PCL_CLUSTER_SIZE = 'INPUT_PCL_CLUSTER_SIZE'
    OUTPUT_RASTER_RESOLUTION = 'OUTPUT_RASTER_RESOLUTION'
    OUTPUT = 'OUTPUT'

    PCL_PARSING_REGEXP = '(?P<header>\w+?)_(?P<stripnumber>\d+?)_(?P<sensor>S\d?)\.(?P<inputformat>las|laz?)'

    def createInstance(self):
        self.messageTag = type(self).__name__ # e.g. string WellsAll
        return WellsAll()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'plantsall'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Hent alle anlæg')
		
    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFile(
                name=self.INPUT_PCL,
                description=self.tr('Input LAS (or LAZ if managed by installed PDAL)'),
                behavior=QgsProcessingParameterFile.File,
                extension=None,
                defaultValue=None,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                name=self.INPUT_CROSSWALK_CLASSES,
                description=self.tr('Input crosswalk classification'),
                defaultValue=None,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterBand(
                name=self.INPUT_CROSSWALK_CLASSES_BAND,
                description=self.tr('Input crosswalk classification band'),
                defaultValue=1,
                parentLayerParameterName=self.INPUT_CROSSWALK_CLASSES,
                optional=False
            )
        )

        # Input intensity ranges ranges. These are for advanced settings
        s1_parameter = QgsProcessingParameterRange(
            name=self.INPUT_PCL_S1_INTENSITY_RANGE,
            description=self.tr('PCL intensity range to extract street signs (Strip1)'),
            type=QgsProcessingParameterNumber.Double,
            defaultValue="3000,3200",
            optional=True
        )
        s1_parameter.setFlags(QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(s1_parameter)

        s2_parameter =  QgsProcessingParameterRange(
            name=self.INPUT_PCL_S2_INTENSITY_RANGE,
            description=self.tr('PCL intensity range to extract street signs (Strip2)'),
            type=QgsProcessingParameterNumber.Double,
            defaultValue="1600,2000",
            optional=True
        )
        s2_parameter.setFlags(QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(s2_parameter)

        # input PCL cluster size. This is advanced setting
        cluster_size_parameter = QgsProcessingParameterNumber(
            name=self.INPUT_PCL_CLUSTER_SIZE,
            description=self.tr('PCL cluster size'),
            type=QgsProcessingParameterNumber.Double,
            defaultValue=50,
            optional=True
        )
        cluster_size_parameter.setFlags(QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(cluster_size_parameter)

        # output rasterization advanced parameters
        output_raster_resolution = QgsProcessingParameterNumber(
            name=self.OUTPUT_RASTER_RESOLUTION,
            description=self.tr('Output raster resolution'),
            type=QgsProcessingParameterNumber.Double,
            defaultValue=0.18,
            optional=True
        )
        output_raster_resolution.setFlags(QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(output_raster_resolution)

        # We add a raster sink in which to store our processed raster (this
        # usually takes the form of a newly created raster layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                name=self.OUTPUT,
                description=self.tr('Output crosswalks')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the imput PCL. Because the File parameter definition
        # is not possibible a multiple select => select e.g. S1 and parse
        # filename to retrieve the generic filename. Assume the PCL name
        # has the following format:
        # <header>_<strip number>_<sensor number>.[las|laz]
        # e.g. regexp as in self.PCL_PARSING_REGEXP
        pcl_single_strip_source = self.parameterAsFile(
            parameters,
            self.INPUT_PCL,
            context
        )
        if pcl_single_strip_source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_PCL))

        # check filename format
        parsed = re.search(self.PCL_PARSING_REGEXP, pcl_single_strip_source)
        if not parsed:
            raise QgsProcessingException("Cannot correctly parse filename " + self.invalidSourceError(parameters, self.INPUT_PCL))
        
        header = parsed['header']
        stripnumber = parsed['stripnumber']
        sensor = parsed['sensor']
        inputformat = parsed['inputformat']
        if not header or not stripnumber or not sensor or not inputformat:
            raise QgsProcessingException("Cannot correctly parse filename " + self.invalidSourceError(parameters, self.INPUT_PCL))
        
        # get crosswalk classification
        input_crosswalk_classes = self.parameterAsRasterLayer(
            parameters,
            self.INPUT_CROSSWALK_CLASSES,
            context
        )
        input_crosswalk_classes_band = self.parameterAsInt(
            parameters,
            self.INPUT_CROSSWALK_CLASSES_BAND,
            context
        )
        nodata = input_crosswalk_classes.dataProvider().sourceNoDataValue(input_crosswalk_classes_band)

        # get pipeline advanced parameters
        inputs_pcl_s1_intensity_range = self.parameterAsRange(
            parameters,
            self.INPUT_PCL_S1_INTENSITY_RANGE,
            context
        )
        inputs_pcl_s2_intensity_range = self.parameterAsRange(
            parameters,
            self.INPUT_PCL_S2_INTENSITY_RANGE,
            context
        )
        inputs_pcl_cluster_size = self.parameterAsDouble(
            parameters,
            self.INPUT_PCL_CLUSTER_SIZE,
            context
        )
        output_raster_resolution = self.parameterAsDouble(
            parameters,
            self.OUTPUT_RASTER_RESOLUTION,
            context
        )

        # gets outputs

        output_raster = self.parameterAsOutputLayer(
            parameters,
            self.OUTPUT,
            context
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(input_crosswalk_classes.crs().authid()))

        #
        # prepare pipelines
        #

        # get PCL filename to process
        s1_filename = "{}_{}_S1.{}".format(header,stripnumber,inputformat)
        s2_filename = "{}_{}_S2.{}".format(header,stripnumber,inputformat)
        sourcepath = os.path.dirname(pcl_single_strip_source)
        s1_path = os.path.join(sourcepath, s1_filename)
        s2_path = os.path.join(sourcepath, s2_filename)
        if not os.path.exists(s1_path) and not os.path.isfile(s1_path):
            raise QgsProcessingException("File {} does not exists or it's not a file".format(s1_path))
        if not os.path.exists(s2_path) and not os.path.isfile(s2_path):
            raise QgsProcessingException("File {} does not exists or it's not a file".format(s2_path))

        # prepare pdal json pipeline changing with input parameters
        pdal_pipeline_path = os.path.join(self.pipelinesPath, 'streetmarks_extraction', 'street_marks_extraction.json')
        with open(pdal_pipeline_path, 'r') as f:
            jsondata = f.readlines()
        # strip all comments that does not part of json standard
        jsondata = [line.strip() for line in jsondata if (not line.strip().startswith('/') and not line.strip().startswith('*') )]
        jsondata = "".join(jsondata)

        try:
            pdal_pipeline = json.loads(jsondata)
        except Exception as ex:
            raise QgsProcessingException(str(ex))

        # set json values
        stage_map = {
            "reader1":0,
            "pmf1":1,
            "reader2":2,
            "pmf1":3,
            "range1":4,
            "range2":5,
            "merge":6,
            "cluster":7,
            "writer":8,
        }

        pdal_pipeline["pipeline"][stage_map['reader1']]['filename'] = s1_path
        pdal_pipeline["pipeline"][stage_map['reader2']]['filename'] = s2_path

        if inputs_pcl_s1_intensity_range is not None:
            intensity_interval = ",Intensity[{}:{}]".format(inputs_pcl_s1_intensity_range[0], inputs_pcl_s1_intensity_range[1])
        else:
            intensity_interval = ""
        pdal_pipeline["pipeline"][stage_map['range1']]['limits'] = 'Classification[2:2]{}'.format(intensity_interval)

        if inputs_pcl_s2_intensity_range is not None:
            intensity_interval = ",Intensity[{}:{}]".format(inputs_pcl_s2_intensity_range[0], inputs_pcl_s2_intensity_range[1])
        else:
            intensity_interval = ""
        pdal_pipeline["pipeline"][stage_map['range2']]['limits'] = 'Classification[2:2]{}'.format(intensity_interval)

        if inputs_pcl_cluster_size is not None:
            pdal_pipeline["pipeline"][stage_map['cluster']]["min_points"] = inputs_pcl_cluster_size
        else:
            pdal_pipeline["pipeline"][stage_map['cluster']].pop("min_points", None)
    
        if output_raster_resolution is not None:
            pdal_pipeline["pipeline"][stage_map['writer']]["resolution"] = output_raster_resolution
        else:
            pdal_pipeline["pipeline"][stage_map['writer']].pop("resolution", None)

        out_raster_path = None
        if isinstance(output_raster, str):
            out_raster_path = output_raster
        else:
            out_raster_path = output_raster.source()
        pdal_pipeline["pipeline"][stage_map['writer']]["filename"] = out_raster_path

        # run pdal pipeline
        # cannot use python-pdal because mosto stable versions just use python-pdal for py2
        # try:
        #     jsondata = json.dumps(pdal_pipeline)
        #     pipeline = pdal.Pipeline(jsondata)
        #     pipeline.validate()
        #     pipeline.loglevel = 4
        #     count = pipeline.execute()
        #     arrays = pipeline.arrays
        #     metadata = pipeline.metadata
        #     log = pipeline.log
        # except Exception as ex:
        #     raise QgsProcessingException(ex)
        jsondata = json.dumps(pdal_pipeline)
        adapted_pipeline_path = QgsProcessingUtils.generateTempFilename('pdalpipeline') + '.json'
        with open(adapted_pipeline_path, 'w+') as f:
            f.write(jsondata)

        # notify phase
        feedback.pushConsoleInfo('Phase1: Rough extract of Crosswalks from PCL')

        res = processing.run("PDALtools:pdalpipelineexecutor", {
            'INPUT_PCL_1':None, 
            'INPUT_PCL_2':None,
            'OUTPUT_PCL':out_raster_path,
            'INPUT_PIPELINE':adapted_pipeline_path},
            context=context,
            feedback=feedback
        )

        # Return the results of the algorithm.
        return {self.OUTPUT: out_raster_path}
