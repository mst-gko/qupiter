# -*- coding: utf-8 -*-

"""
***************************************************************************
    wells_all.py
    -------------------------
    begin                : January 2019
    copyright            : (C) 2019 by Jakob Lanstorp
    email                : jla at geus dot dk
    dev for              : http://geus.dk/qupiter/
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Jakob Lanstorp'
__date__ = 'January 2019'
__copyright__ = '(C) 2019, Jakob Lanstorp'


import os
import re
import json
import subprocess
# import pdal # cannot import pdal because mosto stable versions just use python-pdal for py2
from PyQt5.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterExtent,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterBand,
                       QgsProcessingParameterRange,
                       QgsProcessingParameterNumber,
                       QgsMessageLog,
                       QgsProcessingParameterDefinition,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterVectorDestination,
                       QgsProcessingUtils)
import processing
from ..well_algorithm import WellAlgorithm
from ..pcjupiterxl_aux import PCJupiterXLAux as Aux

class WellsAllExtent(WellAlgorithm):
    """
    Get all wells within a choosen rectangular extent
    """

    INPUT_EXTENT = 'INPUT_EXTENT'
    OUTPUT = 'OUTPUT'

    def createInstance(self):
        self.messageTag = type(self).__name__ # e.g. string WellsAllExtent
        return WellsAllExtent()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'wellsallextent'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Hent alle boringer indenfor valgte extent')
		
    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterExtent(
                name=self.INPUT_EXTENT,
                description=self.tr('Define geometric extent of query'),
                defaultValue="0,0,0,0",
                optional=False
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                name=self.OUTPUT,
                description=self.tr('Output borehole table')
            )
        )
        
    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        output_extent = self.parameterAsExtent(
            parameters,
            self.INPUT_EXTENT,
            context
        )
        
        if output_extent is None or output_extent == '0,0,0,0':
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_EXTENT))
        
        #output_vector = self.parameterAsVectorLayer(
        #    parameters, 
        #    self.OUTPUT, 
        #    context
        #)    
        
        output_vector = self.parameterAsOutputLayer(
            parameters,
            self.OUTPUT,
            context
        )

        # Send some information to the user
        #feedback.pushInfo('CRS is {}'.format(input_crosswalk_classes.crs().authid()))

        out_vector_path = None
        if isinstance(output_vector, str):
            out_vector_path = output_vector
        else:
            out_vector_path = output_vector.source()

        Aux.log_info('output_extent: {}'.format(output_extent))
        Aux.log_info('out_vector_path: {}'.format(out_vector_path))

        # Return the results of the algorithm.
        return {self.OUTPUT: out_vector_path}
